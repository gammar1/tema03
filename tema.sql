drop database if exists tema;
create database tema;

use tema;


	/* Tabele */

create table client(
id int not null primary key auto_increment,
lastname varchar(40) not null,
firstname varchar(40) not null,
username varchar(40) not null,
password varchar(40) not null,
telnumber varchar(40) not null,
mail varchar(50) not null);


create table product(
id int not null primary key auto_increment,
nume varchar(30) not null,
cost float not null,
stock int );

create table orders(
id_product int not null,
id_client int not null,
amount int default 0,
#primary key(id_product));
FOREIGN KEY(id_product) REFERENCES product(id),
FOREIGN KEY(id_client) REFERENCES client(id));

INSERT INTO product(nume, cost,stock)
VALUES( 'mere', 5,65),
('Apa_plata',2.5,122),
('shompun',5.99,52),
('cana',12.25,96),
('suc', 5.99, 100),
( 'banana', 4.99,75);


	/* Client */
INSERT INTO client (lastname, firstname, username,password,telnumber,mail)
VALUES( 'Garyagdyyev', 'Gammar', 'test', 'test', '0745778996', 'gammargaryagdyyev@gmail.com'),
('Test','test','t','t','0789568927','fdsjoi@fsi'),
( 'Velnazarov', 'Myrat', 'tet', 'tet', '0745978996', 'mayrat@gmail.com'),
('Tore','Kerven','da','da','65893','dfsh@gmail.com');




	/* Stoc */
INSERT INTO orders(id_product, id_client, amount)
VALUES(1,1,8);



	