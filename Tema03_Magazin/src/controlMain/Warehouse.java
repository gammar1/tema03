package controlMain;

import java.util.TreeMap;

public class Warehouse implements java.io.Serializable {
	TreeMap<Integer,Products> tree = new TreeMap<Integer, Products>();
	
	public void addProduct(int key, Products p){
		this.tree.put(key, p);
	}
	
	public void deleteProduct(int key){
		this.tree.remove(key);
	}
	
	public TreeMap<Integer, Products> getTree(){
		return tree;
	}
	
}
