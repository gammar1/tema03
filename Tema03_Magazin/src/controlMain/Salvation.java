package controlMain;

import java.beans.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Logger;

import com.mysql.jdbc.PreparedStatement;

import view.View;

public class Salvation {

	
	private Warehouse products = new Warehouse();
	private OPCheck commands = new OPCheck();
	private Users clients = new Users();

	View view;

	private static final Logger LOGGER = Logger.getLogger(Salvation.class.getName());
	private static final String DRIVER = "com.mysql.jdbc.Driver";
	private static final String DBURL = "jdbc:mysql://localhost:3306/tema";
	private static final String USER = "root";
	private static final String PASS = "Gammar1993!";
	Connection con;
	PreparedStatement ps;
	ResultSet rs;

	Salvation() {
		try {
			Class.forName(DRIVER);
			con = DriverManager.getConnection(DBURL, USER, PASS);
			
			ps = (PreparedStatement) con.prepareStatement("");
			int i=0;
			// execute sql query
			rs = ps.executeQuery("select*from product");
			// process the result set
			while (rs.next()) {
				Products p = new Products();
				//System.out.println(rs.getString("nume")+ " , "+rs.getString("cost")+", "+rs.getString("stock")+i);
				String name = rs.getString("nume");
				double cost = Double.parseDouble(rs.getString("cost"));
				int amount = Integer.parseInt(rs.getString("stock"));
				p.setName(name);
				p.setCost(cost);
				p.setAmount(amount);
				products.addProduct(i, p);
				i++;
			}
			 i=0;
			 
			 rs = ps.executeQuery("select*from client");
				// process the result set
				while (rs.next()) {
					Customers c = new Customers();
					//System.out.println(rs.getString("lastname")+","+rs.getString("firstname")+","+rs.getString("username")+
				//			","+rs.getString("mail")+","+rs.getString("password")+","+rs.getString("telnumber"));
					String lastname = rs.getString("lastname");
					String firstname =rs.getString("firstname");
					String user = rs.getString("username");
					String mail = rs.getString("mail");
					String password = rs.getString("password");
					String telnumber = rs.getString("telnumber");
					
					c.setLastname(lastname);
					c.setFirstname(firstname);
					c.setUser(user);
					c.setMail(mail);
					c.setPassword(password);
					c.setTelnumber(telnumber);
					clients.addUser(i, c);
					i++;
				}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Warehouse getProducts() {
		// TODO Auto-generated method stub
		
		return products;
	}

	public OPCheck getCommands() {
		// TODO Auto-generated method stub
		return commands;
	}

	public void setCommands(OPCheck commands) {
		// TODO Auto-generated method stub
		this.commands = commands;

	}

	public void setProducts(Warehouse wh ,int t) {
		// TODO Auto-generated method stub
		
		
		
		try {

			int k = wh.tree.size() -1;
			//System.out.println(k+wh.tree.get(k).getName());
		
		
			if(t == 1){
				ps = (PreparedStatement) con.prepareStatement("insert into product(nume, cost, stock) values(?,?,?)");
	
			String n= wh.tree.get(k).getName();
			  
			float m=(float)wh.tree.get(k).getCost(); 
			int a = wh.tree.get(k).getAmount();
			  
			ps.setString(1,n);  
			ps.setFloat(2,m); 
			ps.setInt(3,a);
			ps.executeUpdate();
			products = wh;
			}else 
				products = wh;
			
			
		
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	public Users getClients() {
		// TODO Auto-generated method stub
		return clients;
	}

	public void setClients(Users users, int t) {
		// TODO Auto-generated method stub
		if(t==1){
			try {
			Class.forName(DRIVER);
			con = DriverManager.getConnection(DBURL, USER, PASS);
			
			ps = (PreparedStatement) con.prepareStatement("INSERT INTO client (lastname, firstname, username,password,telnumber,mail)"+
					"VALUES( ?, ?, ?, ?, ?, ?);");
	
			int  k=users.tree.size()-1;
			
			String lastname =  users.tree.get(k).getLastname();
			String firstname =users.tree.get(k).getFirstname();
			String user = users.tree.get(k).getUser();
			String mail = users.tree.get(k).getMail();
			String password = users.tree.get(k).getPassword();
			String telnumber = users.tree.get(k).getTelnumber();
			
			ps.setString(1,lastname);  
			ps.setString(2,firstname); 
			ps.setString(3,user); 
			ps.setString(4,password); 
			ps.setString(5,telnumber); 
			ps.setString(6,mail); 
			ps.executeUpdate();
			
			this.clients = users;
		} catch (Exception e) {
			e.printStackTrace();
		}
		}else 
			this.clients = users;
		

	}

}
