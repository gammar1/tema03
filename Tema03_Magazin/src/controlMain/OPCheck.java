package controlMain;

import java.io.FileWriter;
import java.io.IOException;
import java.util.TreeMap;

public class OPCheck implements java.io.Serializable {
	TreeMap<Integer, Command> tree = new TreeMap<Integer, Command>();
	public void addCommand(Command com) {
		// TODO Auto-generated method stub
		if(tree.size()<=1) 
			tree.put(0,com);
		else 
			tree.put(tree.lastKey()+1, com);
		
	}

	public void chitanta() {
		// TODO Auto-generated method stub
		Command cm = tree.get(tree.lastKey());
		
		try{
			FileWriter fl = new FileWriter("Chitanta.txt",true);
			
			fl.write("____________________________________________________________\n");
			fl.write("\tName: "+cm.getLastname());
			fl.write("\n\tFirstname: "+cm.getFirstname());
			fl.write("\n____________________________________________________________\n");
			fl.write("\tBought:");
			fl.write(" "+ cm.getProductname()+"  "+cm.getAmount()+" * "+cm.getCost());
			fl.write("\n\t\t\t Total:"+cm.getAmount()*cm.getCost());
			fl.write("\n\n\n\n\n");
			fl.close();
		}catch (IOException e){
			e.printStackTrace();
		}
		
	}

	public TreeMap<Integer, Command> getCommandForUser(String user) {
		// TODO Auto-generated method stub
		TreeMap<Integer, Command> tree2= new TreeMap<Integer, Command>();
		for (int i=0;i<tree.size();i++){
			if (user.equals(tree.get(i).getUser())){
				if (tree2.size()<1) tree2.put(0, tree.get(i));
				else tree2.put(tree2.lastKey()+1, tree.get(i));
			}
		}
		return tree2;
	}

}
