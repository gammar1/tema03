package controlMain;

import java.util.Collection;
import java.util.Iterator;
import java.util.TreeMap;

public class Users implements java.io.Serializable {
	
	TreeMap<Integer, Customers> tree = new TreeMap<Integer, Customers>();
	
	public Users(){
		
	}
	
	public void addUser(int key, Customers c){
		this.tree.put(key, c);
	}
	
	public void deleteProduct(int key){
		this.tree.remove(key);
	}
	
	public Customers getCustomersNumber(int a){
		int i=0;
		Customers user = new Customers();
		
		Collection collection = tree.values();
		
		Iterator itr = collection.iterator();
		
		while(itr.hasNext() && i <= a){
			user = (Customers) itr.next();
			i++;
		}
		
		return user;
	}
	
	public int sizeUsers(){
		return tree.size();
	}
	
	public TreeMap<Integer, Customers> getTree(){
		return tree;
	}

	
}
