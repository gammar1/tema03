package controlMain;

import view.View;

public class MainClass {
	
	public static void main(String[] args) {
		View v = new View();
		Model m = new Model(v);
	}

}
