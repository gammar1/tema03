package controlMain;

public class Verification {
	
	private String searchName;
	private boolean a,b,c,admin = false;
	private int minimAmount,maximAmount, stoc;
	private double minimCost,maximCost;
	

	public void setSearchMode(boolean x, boolean y, boolean z) {
		// TODO Auto-generated method stub
		this.a = x;
		this.b = y;
		this.c = z;
		
	}



	public void setSearchParam(double minimCost, double maximCost, int minimAmount, int maximAmount,
			String searchName) {
		// TODO Auto-generated method stub
		this.minimCost = minimCost;
		this.maximCost = maximCost;
		this.minimAmount = minimAmount;
		this.maximAmount = maximAmount;
		this.searchName = searchName;
		
	}



	public boolean search(String name, double cost, int amount) {
		// TODO Auto-generated method stub
		
		if (this.a && this.b && this.c) {
			if ((this.minimCost <= cost && this.maximCost >= cost)
					&& (this.minimAmount <= amount && this.maximAmount >= amount)
					&& (this.searchName.toUpperCase().equals(name.toUpperCase())))
				return true;
			else
				return false;
		} else if ((this.a && this.b && !this.c)) {
			if ((this.minimCost <= cost && this.maximCost >= cost)
					&& (this.minimAmount <= amount && this.maximAmount >= amount))
				return true;
			else
				return false;
		} else if (this.a && !this.b && this.c) {
			if ((this.minimCost <= cost && this.maximCost >= cost)
					&& (this.searchName.toUpperCase().equals(name.toUpperCase())))
				return true;
			else
				return false;
		} else if (this.a && !this.b && !this.c) {
			if ((this.minimCost <= cost && this.maximCost >= cost))
				return true;
			else
				return false;
		} else if (!this.a && this.b && this.c) {
			if ((this.minimAmount <= amount && this.maximAmount >= amount)
					&& (this.searchName.toUpperCase().equals(name.toUpperCase())))
				return true;
			else
				return false;
		} else if (!this.a && this.b && !this.c) {
			if ((this.minimAmount <= amount && this.maximAmount >= amount))
				return true;
			else
				return false;
		} else if (!this.a && !this.b && this.c) {
			if ((this.searchName.toUpperCase().equals(name.toUpperCase())))
				return true;
			else
				return false;
		} else
			return false;


	}



	public double calculateCost(int requiredAmount, double cost) {
		// TODO Auto-generated method stub
		return (requiredAmount*cost);
	}



	public boolean checkUserExist(String user, String user2) {
		// TODO Auto-generated method stub
		if (user.equals(user2))
			return true;
		else
			return false;
	
	}



	public boolean checkAcces(String user, String password, String user2, String password2) {
		// TODO Auto-generated method stub
		if (user.equals(user2) && password.equals(password2))
			return true;
		else
			return false;
	
	}



	public boolean checkPassword(String password, String password2) {
		// TODO Auto-generated method stub
		if (password.equals(password2))
			return true;
		else
			return false;
	}



	public boolean checkTelNumber(String telnumber) {
		// TODO Auto-generated method stub
		int n = 0, i;
		
			for (i = 0; i < telnumber.length(); i++)
				if (telnumber.charAt(i) <= '9' && telnumber.charAt(i) >= '0')
					n++;
			if (n == 10)
				return true;
			else
				return false;
		
	}



	public boolean checkMail(String mail) {
		// TODO Auto-generated method stub
		int n = 0, i;

		for (i = 0; i < mail.length(); i++)
			if (mail.charAt(i) == '@')
				n++;
		if (n == 1)
			return true;
		else
			return false;
	}

}
