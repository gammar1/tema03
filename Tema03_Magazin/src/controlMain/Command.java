package controlMain;

public class Command {
	private String productname,lastname,firstname,user,mail,telnumber;
	private double cost;
	private int amount;
	public Command(String productname,double cost,int amount,String user, String lastname, String firstname,
			String telnumber, String mail) {
		super();
		this.productname = productname;
		this.lastname = lastname;
		this.firstname = firstname;
		this.user = user;
		this.mail = mail;
		this.telnumber = telnumber;
		this.cost = cost;
		this.amount = amount;
	}
	public String getProductname() {
		return productname;
	}
	public void setProductname(String productname) {
		this.productname = productname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getTelnumber() {
		return telnumber;
	}
	public void setTelnumber(String telnumber) {
		this.telnumber = telnumber;
	}
	public double getCost() {
		return cost;
	}
	public void setCost(double cost) {
		this.cost = cost;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	

}
