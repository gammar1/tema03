package controlMain;

public class Customers {
	
	private String lastname,firstname,user,password,telnumber,mail;
	private boolean admin;
	
	public Customers() {
	}
	public Customers(String lastname, String firstname, String user, String password, String telnumber, String mail) {
		super();
		this.lastname = lastname;
		this.firstname = firstname;
		this.user = user;
		this.password = password;
		this.telnumber = telnumber;
		this.mail = mail;
	//	this.admin = true;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getTelnumber() {
		return telnumber;
	}
	public void setTelnumber(String telnumber) {
		this.telnumber = telnumber;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public boolean isAdmin() {
		return admin;
	}
	public void setAdmin() {
		this.admin = true;
	}

	
	

}
