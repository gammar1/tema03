package controlMain;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeMap;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import view.View;

public class Model {
	Salvation sl = new Salvation();
	JFrame info;
	View view;
	Warehouse wh = new Warehouse();
	Verification vr = new Verification();
	Users users = new Users();
	Products pr;
	Customers cm;
	boolean singin = false;
	boolean admin = false;
	int index = 0;
	Command com;
	OPCheck commands = new OPCheck();

	public Model(View v) {
		this.view = v;
		this.view.addProductsListeners(new openProducts());
		this.view.addContactListeners(new openContact());
		this.view.addSingInPageListeners(new openSingIn());
		this.view.addAdminPageListeners(new openAdminPage());
		this.view.addSearchListeners(new SearchProducts());
		this.view.addCostCommand(new calculateCost());
		this.view.addCommand(new command());
		this.view.addAddProducts(new AddProducts());
		this.view.addSubProducts(new subtractProduct());
		this.view.addTrainingUpdateListeners(new trainingUpdateProducts());
		this.view.addTrainingUpdateCLNLs(new trainingUpdateClients());
		this.view.addSingIn(new verificationSingIn());
		this.view.addNewAccount(new createAccount());
		this.view.addSingOut(new singOut());
		this.view.addAdminProducts(new adminProductsPage());
		this.view.addModificationProduct(new modificateProduct());
		this.view.addupdateClient(new modificateUser());
		this.view.addMyPageListeners(new openMyPage());
		this.view.addAddClient(new addClient());
		this.view.setListener(listeners);
		this.view.addNewPage(new newPage());
	}

	class addClient implements ActionListener {

		public void actionPerformed(ActionEvent e) {

			users.addUser(users.sizeUsers(), new Customers(view.getTname().getText(), view.getFFirstname().getText(),
					view.getFUser().getText(), "parola", view.getFTelnumber().getText(), view.getFmail().getText()));
			sl.setClients(users, 1);
			view.setNullTable4();
			users = sl.getClients();

			Collection cl = users.getTree().values();
			Iterator it = cl.iterator();
			Integer i = 0;
			Set<Integer> keys = users.getTree().keySet();
			Iterator it2 = keys.iterator();

			while (it.hasNext()) {
				i = (Integer) it2.next();
				// view.setNewRowAdmin((int) i, (Products) it.next());

				it.next();

				view.setNewRow4(users.getCustomersNumber(i).getLastname(), users.getCustomersNumber(i).getFirstname(),
						users.getCustomersNumber(i).getUser(), users.getCustomersNumber(i).getTelnumber(),
						users.getCustomersNumber(i).getMail());
			}

		}
	}

	class newPage implements ActionListener {

		public void actionPerformed(ActionEvent e) {

			view.setVisAdminProduct(false);
			view.setVisHome(false);
			view.setVisContact(false);
			view.setVisMyPage(false);
			view.setVisAdmin(false);
			view.setVisProductsPage(false);
			view.setVisSingIn(false);
			view.setVisNewPage(true);

			view.setColorPage1(0);
			view.setColorPage2(1);
			view.setColorPage3(0);
			view.setColorPage4(0);

			view.setColorPageB1(0);
			view.setColorPageB2(1);
			view.setColorPageB3(0);
			view.setColorPageB4(0);
			view.setColorPageB5(0);

			view.setNullTable4();

			users = sl.getClients();

			Collection cl = users.getTree().values();
			Iterator it = cl.iterator();
			Integer i = 0;
			Set<Integer> keys = users.getTree().keySet();
			Iterator it2 = keys.iterator();

			while (it.hasNext()) {
				i = (Integer) it2.next();
				// view.setNewRowAdmin((int) i, (Products) it.next());

				it.next();

				view.setNewRow4(users.getCustomersNumber(i).getLastname(), users.getCustomersNumber(i).getFirstname(),
						users.getCustomersNumber(i).getUser(), users.getCustomersNumber(i).getTelnumber(),
						users.getCustomersNumber(i).getMail());
			}

		}
	}

	class SearchProducts implements ActionListener {

		public void actionPerformed(ActionEvent e) {

			view.setNullTable();
			wh = sl.getProducts();

			boolean x, y, z;
			x = view.getRDBCost();
			y = view.getRDBAmount();
			z = view.getRDBName();

			vr.setSearchMode(x, y, z);

			Collection cl = wh.getTree().values();
			Iterator it = cl.iterator();
			Integer i = 0;
			Set<Integer> keys = wh.getTree().keySet();
			Iterator it2 = keys.iterator();

			while (it.hasNext()) {
				i = (Integer) it2.next();
				pr = (Products) it.next();
				vr.setSearchParam(view.getMinimCost(), view.getMaximCost(), view.getMinimAmount(),
						view.getMaximAmount(), view.getSearchName());
				if (vr.search(pr.getName(), pr.getCost(), pr.getAmount()))
					view.setNewRow((int) i, pr);
			}

		}

	}

	class openProducts implements ActionListener {

		public void actionPerformed(ActionEvent e) {

			view.setVisAdminProduct(false);
			view.setVisHome(false);
			view.setVisContact(false);
			view.setVisMyPage(false);
			view.setVisAdmin(false);
			view.setVisProductsPage(true);
			view.setVisSingIn(false);
			view.setVisNewPage(false);

			view.setColorPage1(0);
			view.setColorPage2(1);
			view.setColorPage3(0);
			view.setColorPage4(0);

			view.setColorPageB1(0);
			view.setColorPageB2(1);
			view.setColorPageB3(0);
			view.setColorPageB4(0);
			view.setColorPageB5(0);

			view.setNullTable();
			wh = sl.getProducts();

			Collection c = wh.getTree().values();
			Iterator it = c.iterator();
			Integer i = 0;
			Set<Integer> keys = wh.getTree().keySet();
			Iterator it2 = keys.iterator();

			while (it.hasNext()) {
				i = (Integer) it2.next();
				view.setNewRow((int) i, (Products) it.next());
			}

		}
	}

	class openAdminPage implements ActionListener {

		public void actionPerformed(ActionEvent e) {

			view.setVisAdminProduct(false);
			view.setVisHome(false);
			view.setVisContact(false);
			view.setVisMyPage(false);
			view.setVisAdmin(true);
			view.setVisProductsPage(false);
			view.setVisSingIn(false);
			view.setVisNewPage(false);

			view.setColorPage1(0);
			view.setColorPage2(0);
			view.setColorPage3(0);
			view.setColorPage4(1);

			view.setColorPageB1(0);
			view.setColorPageB2(0);
			view.setColorPageB3(0);
			view.setColorPageB4(0);
			view.setColorPageB5(1);

		}
	}

	class openContact implements ActionListener {

		public void actionPerformed(ActionEvent e) {

			view.setVisAdminProduct(false);
			view.setVisHome(false);
			view.setVisContact(true);
			view.setVisMyPage(false);
			view.setVisAdmin(false);
			view.setVisProductsPage(false);
			view.setVisSingIn(false);
			view.setVisNewPage(false);

			view.setColorPage1(0);
			view.setColorPage2(0);
			view.setColorPage3(1);
			view.setColorPage4(0);

			view.setColorPageB1(0);
			view.setColorPageB2(0);
			view.setColorPageB3(1);
			view.setColorPageB4(0);
			view.setColorPageB5(0);
		}
	}

	class openSingIn implements ActionListener {

		public void actionPerformed(ActionEvent e) {

			view.setLogat(true);

			view.setVisAdminProduct(false);
			view.setVisHome(false);
			view.setVisContact(false);
			view.setVisMyPage(false);
			view.setVisAdmin(false);
			view.setVisProductsPage(false);
			view.setVisSingIn(true);
			view.setVisNewPage(false);

			view.setColorPage1(1);
			view.setColorPage2(0);
			view.setColorPage3(0);
			view.setColorPage4(0);

			view.setColorPageB1(1);
			view.setColorPageB2(0);
			view.setColorPageB3(0);
			view.setColorPageB4(0);
			view.setColorPageB5(0);

		}
	}

	class calculateCost implements ActionListener {

		public void actionPerformed(ActionEvent e) {

			wh = sl.getProducts();

			int k = view.getRequiredAmount();

			Collection c = wh.getTree().values();
			Iterator it = c.iterator();
			Integer i = 0;
			Set<Integer> keys = wh.getTree().keySet();
			Iterator it2 = keys.iterator();

			while (it.hasNext()) {
				i = (Integer) it2.next();
				pr = (Products) it.next();
				if (i == view.getIdRequired())
					break;
			}
			view.setCostCalculat(vr.calculateCost(view.getRequiredAmount(), pr.getCost()));
		}
	}

	class command implements ActionListener {

		public void actionPerformed(ActionEvent e) {

			view.setNullTable();
			wh = sl.getProducts();
			commands = sl.getCommands();

			Collection c = wh.getTree().values();
			Iterator it = c.iterator();
			Integer k = 0;
			Set<Integer> keys = wh.getTree().keySet();
			Iterator it2 = keys.iterator();

			while (it.hasNext()) {
				k = (Integer) it2.next();
				view.setNewRow((int) k, (Products) it.next());

			}

			///////////////////////////////////// verificare daca Exist ID-ul

			if (!singin)
				JOptionPane.showMessageDialog(info, "Must be singin if you want to buy something!");
			else {

				view.setNullTable();
				int save = wh.getTree().get(view.getIdRequired()).getAmount();
				if (save < view.getRequiredAmount())
					JOptionPane.showMessageDialog(info, "WE don't have enough Products!");
				else {

					double cost = wh.getTree().get(view.getIdRequired()).getCost();
					String name = wh.getTree().get(view.getIdRequired()).getName();
					wh.deleteProduct(view.getIdRequired());
					wh.addProduct(view.getIdRequired(), new Products(name, cost, save - view.getRequiredAmount()));

					String username = "";
					String firstname = "";
					String telnumber = "";
					String usermail = "";

					int i;
					for (i = 0; i < users.sizeUsers(); i++)
						if (vr.checkUserExist(users.getCustomersNumber(i).getUser(), view.getUser())) {

							username = users.getCustomersNumber(i).getLastname();
							firstname = users.getCustomersNumber(i).getFirstname();
							telnumber = users.getCustomersNumber(i).getTelnumber();
							usermail = users.getCustomersNumber(i).getMail();
							i = users.sizeUsers() + 1;
						}

					com = new Command(name, cost, view.getRequiredAmount(), view.getUser(), username, firstname,
							telnumber, usermail);
					commands.addCommand(com);

					c = wh.getTree().values();
					it = c.iterator();
					k = 0;
					keys = wh.getTree().keySet();
					it2 = keys.iterator();
					commands.chitanta();

					while (it.hasNext()) {
						k = (Integer) it2.next();
						view.setNewRow((int) k, (Products) it.next());

					}
				}
			}

			sl.setCommands(commands);
			// sl.setProducts(wh);

		}
	}

	class verificationSingIn implements ActionListener {

		public void actionPerformed(ActionEvent e) {

			String u = view.getUser();
			String password = view.getPassword();

			users = sl.getClients();

			int i, ok = 0;
			for (i = 0; i < users.sizeUsers(); i++)
				if (vr.checkAcces(users.getCustomersNumber(i).getUser(), users.getCustomersNumber(i).getPassword(),
						view.getUser(), password)) {
					if (users.getCustomersNumber(i).getFirstname().equals("Gammar")) {
						admin = true;
					} else
						admin = false;

					ok = 1;
				}

			if (ok == 1) {
				singin = true;
				view.setVisInreg(false);
				view.setVisLog(false);
				view.setSingInPanel(true);
				view.setAccountSingin(u);

				if (admin == true) {
					view.setVisMyPage(false);
					view.setVisMyPageButton(false);
					view.setVisButtonAdmin(true);
					view.setVizButtonMP(false);
				}
			} else
				JOptionPane.showMessageDialog(info, "User name or password is incorrect!");
		}
	}

	class createAccount implements ActionListener {

		public void actionPerformed(ActionEvent e) {

			String lastname = view.getNewAccountName();
			String firstname = view.getNewAccountFirstname();
			String user = view.getNewUserAccount();
			String password = view.getNewAccountPassword();
			String password2 = view.getNewAccountPassword2();
			String mail = view.getNewAccountMail();
			String telnumber = view.getNewAccountTelNumber();

			users = sl.getClients();

			if (!(!(password.equals("")) && !(password2.equals("")) && !(lastname.equals("")) && !(firstname.equals(""))
					&& !(user.equals("")) && !(mail.equals("")) && !(mail.equals(""))))
				JOptionPane.showMessageDialog(info, "Please complete all!");
			else {

				int i, ok = 0;
				for (i = 0; i < users.sizeUsers(); i++)
					if (vr.checkUserExist(users.getCustomersNumber(i).getUser(), user))
						ok = 1;
				if (ok == 1)
					JOptionPane.showMessageDialog(info, "This USER is already exist!");
				else {
					if (!(vr.checkPassword(password, password2)))
						JOptionPane.showMessageDialog(info, "Not same PASSWORDS!");
					else {
						if (!vr.checkTelNumber(telnumber))
							JOptionPane.showMessageDialog(info, "TelNumber is wrong!");
						else {
							if (!vr.checkMail(mail))
								JOptionPane.showMessageDialog(info, "MAIL is not valid!");
							else {
								JOptionPane.showMessageDialog(info, "Registration is SUCCEED!");
								users.addUser(users.sizeUsers() + 1,
										new Customers(lastname, firstname, user, password, telnumber, mail));
							}
						}
					}
				}
			}

			sl.setClients(users, 1);

		}
	}

	class singOut implements ActionListener {

		public void actionPerformed(ActionEvent e) {

			singin = false;
			view.setVisInreg(true);
			view.setVisLog(true);
			view.setSingInPanel(false);
			view.setVisMyPage(false);
			view.setVisNewPage(false);

			admin = false;
			singin = false;
			view.setVisAdmin(false);
			view.setVisMyPage(false);
			view.setVisMyPageButton(true);
			view.setVisButtonAdmin(false);
			view.setVizButtonMP(true);

		}
	}

	class AddProducts implements ActionListener {

		public void actionPerformed(ActionEvent e) {

			view.setNullTable2();
			Collection cl = wh.getTree().values();
			Iterator it = cl.iterator();
			Integer i = 0;
			Set<Integer> keys = wh.getTree().keySet();
			Iterator it2 = keys.iterator();

			while (it.hasNext()) {
				i = (Integer) it2.next();
				view.setNewRowAdmin((int) i, (Products) it.next());
			}
			sl.setProducts(wh, 0);

			wh = sl.getProducts();

			//////////////////////////////////////////////// verificare codul
			//////////////////////////////////////////////// unic ?????
			if (view.getNewProductAmount() > 1000)
				JOptionPane.showMessageDialog(info, "Too much Amount!");
			else {
				view.setNullTable2();
				wh.addProduct(view.getNewProductID(),
						new Products(view.getNewProductName(), view.getNewProductCost(), view.getNewProductAmount()));
				cl = wh.getTree().values();
				it = cl.iterator();
				i = 0;
				keys = wh.getTree().keySet();
				it2 = keys.iterator();

				while (it.hasNext()) {
					i = (Integer) it2.next();
					view.setNewRowAdmin((int) i, (Products) it.next());
				}
				sl.setProducts(wh, 1);

			}
		}
	}

	class adminProductsPage implements ActionListener {

		public void actionPerformed(ActionEvent e) {

			view.setVisAdmin(false);
			view.setVisAdminProduct(true);

			view.setNullTable2();

			wh = sl.getProducts();

			Collection c = wh.getTree().values();
			Iterator it = c.iterator();
			Integer i = 0;
			Set<Integer> keys = wh.getTree().keySet();
			Iterator it2 = keys.iterator();

			while (it.hasNext()) {
				i = (Integer) it2.next();
				view.setNewRowAdmin((int) i, (Products) it.next());
			}

		}
	}

	class subtractProduct implements ActionListener {

		public void actionPerformed(ActionEvent e) {

			view.setVisAdmin(false);
			view.setVisAdminProduct(true);

			view.setNullTable2();
			wh = sl.getProducts();

			Collection c = wh.getTree().values();
			Iterator it = c.iterator();
			Integer i = 0;
			Set<Integer> keys = wh.getTree().keySet();
			Iterator it2 = keys.iterator();

			while (it.hasNext()) {
				i = (Integer) it2.next();
				view.setNewRowAdmin((int) i, (Products) it.next());
			}

			view.setNullTable2();
			int k = view.getIdDeleteProduct();
			wh.deleteProduct(k);

			c = wh.getTree().values();
			it = c.iterator();
			i = 0;
			keys = wh.getTree().keySet();
			it2 = keys.iterator();

			while (it.hasNext()) {
				i = (Integer) it2.next();
				view.setNewRowAdmin((int) i, (Products) it.next());
			}

			sl.setProducts(wh, 0);

		}
	}

	//////////////////////////// //////////verificare daca exist
	class trainingUpdateProducts implements ActionListener {

		public void actionPerformed(ActionEvent e) {

			index = view.getIdREmove();

			Collection c = wh.getTree().values();
			Iterator it = c.iterator();
			Integer i = 0;
			Set<Integer> keys = wh.getTree().keySet();
			Iterator it2 = keys.iterator();

			while (it.hasNext()) {
				i = (Integer) it2.next();
				pr = (Products) it.next();
				if (i == index)
					break;
			}

			view.setShowCantit(pr.getAmount());
			view.setShowID(index);
			view.setShowNumeProdus(pr.getName());
			view.setShowPret(pr.getCost());

		}
	}

	/////////////////////////// //////////client
	/////////////////////////// //////////////////////////////////////////////////////////////////////
	class trainingUpdateClients implements ActionListener {

		public void actionPerformed(ActionEvent e) {

			index = view.getIdUpdate();

			// System.out.println(index);
			users = sl.getClients();
			Collection c = users.getTree().values();
			Iterator it = c.iterator();
			Integer i = 0;
			Set<Integer> keys = users.getTree().keySet();
			Iterator it2 = keys.iterator();

			while (it.hasNext()) {
				i = (Integer) it2.next();
				// users= (Users)
				it.next();
				if (i == index)
					break;
			}

			view.setShowname1(users.getTree().get(i).getLastname());
			view.setShowFirstname1(users.getTree().get(i).getFirstname());
			view.setShowUser1(users.getTree().get(i).getUser());
			view.getShowMail1(users.getTree().get(i).getMail());
			view.setShowTel1(users.getTree().get(i).getTelnumber());

		}
	}

	class modificateProduct implements ActionListener {

		public void actionPerformed(ActionEvent e) {

			view.setNullTable2();

			Collection c = wh.getTree().values();
			Iterator it = c.iterator();
			Integer i = 0;
			Set<Integer> keys = wh.getTree().keySet();
			Iterator it2 = keys.iterator();

			while (it.hasNext()) {
				i = (Integer) it2.next();
				view.setNewRowAdmin((int) i, (Products) it.next());
			}

			wh = sl.getProducts();

			index = view.getIdREmove();

			///////////////// ///////////////verificare index
			if (view.getShowAmount() > 1000)
				JOptionPane.showMessageDialog(info, "Too much Amount!");
			else {
				wh.deleteProduct(index);
				wh.addProduct(view.getShowID(),
						new Products(view.getShowProductName(), view.getShowCost(), view.getShowAmount()));
				view.setNullTable2();

				i = 0;
				c = wh.getTree().values();
				it = c.iterator();
				keys = wh.getTree().keySet();
				it2 = keys.iterator();

				while (it.hasNext()) {
					i = (Integer) it2.next();
					view.setNewRowAdmin((int) i, (Products) it.next());
				}
			}
			sl.setProducts(wh, 0);

		}
	}

	//////////////////////////////////////////////////// modificare
	//////////////////////////////////////////////////// user////////////////////////////////////////////////////////////////
	class modificateUser implements ActionListener {

		public void actionPerformed(ActionEvent e) {

			String lastname = view.getShowname1();
			String firstname = view.getShowFirstname1();
			String user = view.getShowUser1();
			String mail = view.getShowMail1();
			String telnumber = view.getShowTel1();

			users = sl.getClients();

			if (!(!(lastname.equals("")) && !(firstname.equals("")) && !(user.equals("")) && !(mail.equals(""))
					&& !(mail.equals(""))))
				JOptionPane.showMessageDialog(info, "Please complete all!");
			else {

				int i, ok = 0;
				for (i = 0; i < users.sizeUsers(); i++)
					if (vr.checkUserExist(users.getCustomersNumber(i).getUser(), user))
						ok = 1;
				if (ok == 1)
					JOptionPane.showMessageDialog(info, "This USER is already exist!");
				else {

					if (!vr.checkTelNumber(telnumber))
						JOptionPane.showMessageDialog(info, "TelNumber is wrong!");
					else {
						if (!vr.checkMail(mail))
							JOptionPane.showMessageDialog(info, "MAIL is not valid!");
						else {
							JOptionPane.showMessageDialog(info, "Registration is SUCCEED!");
				
							
							 System.out.println(index);
							users.getTree().get(index).setLastname(lastname) ;
							users.getTree().get(index).setFirstname(firstname);
							users.getTree().get(index).setUser(user);
							users.getTree().get(index).setMail(mail);
							users.getTree().get(index).setTelnumber(telnumber);

							
							sl.setClients(users, 0);
							
							System.out.println(4949849);
							view.setNullTable4();
							users = sl.getClients();

							Collection cl = users.getTree().values();
							Iterator it = cl.iterator();
							
							Set<Integer> keys = users.getTree().keySet();
							Iterator it2 = keys.iterator();

							while (it.hasNext()) {
								i = (Integer) it2.next();
								// view.setNewRowAdmin((int) i, (Products) it.next());

								it.next();

								view.setNewRow4(users.getCustomersNumber(i).getLastname(), users.getCustomersNumber(i).getFirstname(),
										users.getCustomersNumber(i).getUser(), users.getCustomersNumber(i).getTelnumber(),
										users.getCustomersNumber(i).getMail());
							}
						}
					}
				}
			}

			

		}
	}

	class openMyPage implements ActionListener {

		public void actionPerformed(ActionEvent e) {

			view.setVisAdminProduct(false);
			view.setVisHome(false);
			view.setVisContact(false);
			view.setVisMyPage(true);
			view.setVisAdmin(false);
			view.setVisProductsPage(false);
			view.setVisSingIn(false);
			view.setVisNewPage(false);

			view.setColorPage1(0);
			view.setColorPage2(0);
			view.setColorPage3(0);
			view.setColorPage4(1);

			view.setColorPageB1(0);
			view.setColorPageB2(0);
			view.setColorPageB3(0);
			view.setColorPageB4(1);
			view.setColorPageB5(0);

			commands = sl.getCommands();
			TreeMap<Integer, Command> t = new TreeMap<Integer, Command>();
			t = commands.getCommandForUser(view.getUser());

			view.setNullTable3();
			int m = 0;
			for (int i = 0; i < t.size(); i++) {
				if (t.get(i).getUser().equals(view.getUser())) {
					pr = new Products(t.get(i).getProductname(), t.get(i).getCost(), t.get(i).getAmount());
					m++;
					view.setNewRow3(m, pr);
				}
			}
		}
	}

	WindowListener listeners = new WindowAdapter() {
		public void windowClosing(WindowEvent w) {
			// s.Serializari();
		}
	};

}
