package view;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.Font;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.awt.event.ActionEvent;
import javax.swing.JPasswordField;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.JEditorPane;
import java.awt.Color;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.JPanel;
import java.awt.SystemColor;
import javax.swing.table.DefaultTableModel;
import java.awt.event.WindowListener;
import javax.swing.JRadioButton;
import javax.swing.table.TableModel;

import controlMain.Products;

import java.awt.Component;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class View {

	private boolean adminn = false;
	private boolean logat = false;
	private JFrame frame;
	private JPasswordField password;

	private JScrollPane scrollP, scrollP1, scrollP2, scrollP4;
	private JTable table, table2, table3, table4;
	private DefaultTableModel dtm, dtm2, dtm3, dtm4;
	private JRadioButton radioBtnCost, radioBtnAmount, radioBtnName;

	private JPanel p1, p2, p3, p4, home, windowsSingUp, windowsSingIn, singInPage, producePage, adminPage, contactPage,
			myPage, singInPanel, produce, customer;

	private JButton singIn, btnContactPage, btnSingInPage, btnAdminPage, btnTainingUpdate, btnAddProduce, btnSubProduce,
			btnFinalUpdate, btnUpdate, btnMyPage, btnProducePage, btnCostTotal, createCont, btnSearch, btnCommand,
			btnAdminAccounts, btnSingOut, btnAdminProduce, btnAddCustomer, btnNewButton;

	private JTextField tfuser, minim, maxim, minimAmount, maximAmount, nameOfSearch, tfId, tfCost, tfAmount,
			tfNewNameAccount, tfNewFirstNameAccount, tfUsersNewAccount, tfNewAccountPassword, tfNewAccountPassword2,
			tfNewAccountMail, tfNewAccountTel, tfAccountSingIn, tfAddNewProduceName, tfAddNewProduceCost,
			tfAddNewProduceAmount, tfNewIdOfProducts, tfIdOfDeleteProduct, tfModifiedProducts, tfName, tfFirstName,
			tfuser1, tftel, tftraining, tfmail, tfname1, tffirstname1, tfuser2, tftel1, tfmail1, tfidproductupdate,
			tfmodifyproductcost, tfmodifyproductname, tfmodifyproductamount;

	private JLabel homeImage, label;

	public View() {

		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {

		String columnNames[] = { "ID.", "Products", "Cost", "Amount" };
		frame = new JFrame("ONLINE SHOPPING");
		frame.getContentPane().setBackground(SystemColor.inactiveCaption);

		frame.setBounds(100, 100, 745, 485);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		customer = new JPanel();
		customer.setBackground(SystemColor.inactiveCaption);
		customer.setBounds(10, 50, 710, 385);
		frame.getContentPane().add(customer);
		customer.setLayout(null);
		customer.setVisible(false);

		label = new JLabel("Clients :");
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setFont(new Font("Tahoma", Font.BOLD, 20));
		label.setBounds(240, 10, 185, 35);
		customer.add(label);

		String columnNames4[] = { "Last name", "First name", "User", "Telefon", "Mail" };

		dtm4 = new DefaultTableModel(columnNames4, 0);

		table4 = new JTable(dtm4);
		table4.setFillsViewportHeight(true);
		table4.setEnabled(false);
		scrollP4 = new JScrollPane(table4);
		scrollP4.setBounds(30, 60, 655, 145);
		customer.add(scrollP4);

		btnAddCustomer = new JButton("Add Client");
		btnAddCustomer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnAddCustomer.setBackground(Color.WHITE);
		btnAddCustomer.setBounds(530, 240, 170, 25);
		customer.add(btnAddCustomer);

		tfName = new JTextField();
		tfName.setBounds(35, 240, 85, 20);
		customer.add(tfName);
		tfName.setColumns(10);

		tfFirstName = new JTextField();
		tfFirstName.setColumns(10);
		tfFirstName.setBounds(135, 240, 85, 20);
		customer.add(tfFirstName);

		tfuser1 = new JTextField();
		tfuser1.setColumns(10);
		tfuser1.setBounds(230, 240, 85, 20);
		customer.add(tfuser1);

		tftel = new JTextField();
		tftel.setColumns(10);
		tftel.setBounds(325, 240, 85, 20);
		customer.add(tftel);

		tfmail = new JTextField();
		tfmail.setColumns(10);
		tfmail.setBounds(420, 240, 85, 20);
		customer.add(tfmail);

		label = new JLabel("Last name:");
		label.setBounds(30, 215, 45, 15);
		customer.add(label);

		label = new JLabel("First name:");
		label.setBounds(130, 215, 45, 15);
		customer.add(label);

		label = new JLabel("User:");
		label.setBounds(225, 215, 45, 15);
		customer.add(label);

		label = new JLabel("Telefon:");
		label.setBounds(320, 215, 45, 15);
		customer.add(label);

		label = new JLabel("Mail:");
		label.setBounds(420, 215, 45, 15);
		customer.add(label);

		btnNewButton = new JButton("Training Update");
		btnNewButton.setBackground(Color.WHITE);
		btnNewButton.setBounds(530, 285, 170, 25);
		customer.add(btnNewButton);

		btnUpdate = new JButton("Update");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnUpdate.setBackground(Color.WHITE);
		btnUpdate.setBounds(530, 335, 170, 25);
		customer.add(btnUpdate);

		tftraining = new JTextField();
		tftraining.setBounds(420, 285, 85, 20);
		customer.add(tftraining);
		tftraining.setColumns(10);

		label = new JLabel("ID pr. pt modificare");
		label.setBounds(250, 290, 160, 15);
		customer.add(label);

		label = new JLabel("Last name:");
		label.setBounds(30, 315, 45, 15);
		customer.add(label);

		tfname1 = new JTextField();
		tfname1.setBounds(35, 335, 85, 20);
		customer.add(tfname1);
		tfname1.setColumns(10);

		tffirstname1 = new JTextField();
		tffirstname1.setBounds(135, 335, 85, 20);
		customer.add(tffirstname1);
		tffirstname1.setColumns(10);

		tfuser2 = new JTextField();
		tfuser2.setBounds(230, 335, 85, 20);
		customer.add(tfuser2);
		tfuser2.setColumns(10);

		tftel1 = new JTextField();
		tftel1.setBounds(325, 335, 85, 20);
		customer.add(tftel1);
		tftel1.setColumns(10);

		tfmail1 = new JTextField();
		tfmail1.setBounds(420, 335, 85, 20);
		customer.add(tfmail1);
		tfmail1.setColumns(10);

		label = new JLabel("First name:");
		label.setBounds(135, 315, 45, 15);
		customer.add(label);

		label = new JLabel("User:");
		label.setBounds(225, 315, 45, 15);
		customer.add(label);

		label = new JLabel("Telefon:");
		label.setBounds(320, 315, 45, 15);
		customer.add(label);

		label = new JLabel("Mail:");
		label.setBounds(420, 315, 45, 15);
		customer.add(label);
		customer.setVisible(false);

		contactPage = new JPanel();
		contactPage.setBackground(SystemColor.inactiveCaption);
		contactPage.setBounds(10, 50, 705, 385);
		frame.getContentPane().add(contactPage);
		contactPage.setLayout(null);

		label = new JLabel("Magazin online");
		label.setFont(new Font("Tahoma", Font.BOLD, 15));
		label.setBounds(225, 35, 270, 25);
		contactPage.add(label);

		label = new JLabel("Pentru nelamuriri ,probleme cu magazinul sau detalii comenzi ");
		label.setBounds(55, 80, 395, 15);
		contactPage.add(label);

		label = new JLabel("Ne puteti contacta la adresa de mail: onlineshoping@gmail.com .");
		label.setBounds(55, 100, 440, 15);
		contactPage.add(label);

		JPanel home2 = new JPanel();
		home2.setBackground(SystemColor.inactiveCaption);
		label = new JLabel();
		try {
			label = new JLabel(new ImageIcon(ImageIO.read(new File("images/onlineshop.jpg"))));
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		/////////////

		label.setBounds(60, 140, 600, 250);
		contactPage.add(label);
		contactPage.setVisible(false);

		produce = new JPanel();
		produce.setBackground(SystemColor.inactiveCaption);
		produce.setBounds(5, 50, 705, 385);
		frame.getContentPane().add(produce);
		produce.setLayout(null);
		produce.setVisible(false);

		label = new JLabel("Products List");
		label.setFont(new Font("Tahoma", Font.BOLD, 18));
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setBounds(245, 10, 205, 35);
		produce.add(label);

		btnAddProduce = new JButton("Add Product");
		btnAddProduce.setBackground(SystemColor.window);
		btnAddProduce.setFont(new Font("Tahoma", Font.BOLD, 9));
		btnAddProduce.setBounds(25, 230, 125, 30);
		produce.add(btnAddProduce);

		btnSubProduce = new JButton("Delete Product");
		btnSubProduce.setBackground(SystemColor.window);
		btnSubProduce.setFont(new Font("Tahoma", Font.BOLD, 9));
		btnSubProduce.setBounds(25, 270, 125, 30);
		produce.add(btnSubProduce);

		btnTainingUpdate = new JButton("Training for Update");
		btnTainingUpdate.setBackground(SystemColor.window);
		btnTainingUpdate.setFont(new Font("Tahoma", Font.BOLD, 9));
		btnTainingUpdate.setBounds(25, 310, 125, 30);
		produce.add(btnTainingUpdate);

		btnFinalUpdate = new JButton("Finaliz. Update");
		btnFinalUpdate.setBackground(SystemColor.window);
		btnFinalUpdate.setFont(new Font("Tahoma", Font.BOLD, 9));
		btnFinalUpdate.setBounds(25, 350, 125, 30);
		produce.add(btnFinalUpdate);

		label = new JLabel("Name:");
		label.setFont(new Font("Tahoma", Font.BOLD, 14));
		label.setBounds(160, 230, 45, 30);
		produce.add(label);

		tfAddNewProduceName = new JTextField();
		tfAddNewProduceName.setBounds(210, 235, 65, 20);
		produce.add(tfAddNewProduceName);
		tfAddNewProduceName.setColumns(10);

		label = new JLabel("Cost:");
		label.setFont(new Font("Tahoma", Font.BOLD, 14));
		label.setBounds(285, 230, 45, 30);
		produce.add(label);

		tfAddNewProduceCost = new JTextField();
		tfAddNewProduceCost.setColumns(10);
		tfAddNewProduceCost.setBounds(330, 235, 65, 20);
		produce.add(tfAddNewProduceCost);

		label = new JLabel("Amount:");
		label.setFont(new Font("Tahoma", Font.BOLD, 14));
		label.setBounds(405, 230, 45, 30);
		produce.add(label);

		tfAddNewProduceAmount = new JTextField();
		tfAddNewProduceAmount.setColumns(10);
		tfAddNewProduceAmount.setBounds(460, 235, 65, 20);
		produce.add(tfAddNewProduceAmount);

		label = new JLabel("ID (unique):");
		label.setFont(new Font("Tahoma", Font.BOLD, 14));
		label.setBounds(535, 230, 80, 30);
		produce.add(label);

		tfNewIdOfProducts = new JTextField();
		tfNewIdOfProducts.setColumns(10);
		tfNewIdOfProducts.setBounds(615, 235, 65, 20);
		produce.add(tfNewIdOfProducts);

		label = new JLabel("Id product:");
		label.setFont(new Font("Tahoma", Font.BOLD, 14));
		label.setBounds(160, 270, 115, 30);
		produce.add(label);

		tfIdOfDeleteProduct = new JTextField();
		tfIdOfDeleteProduct.setColumns(10);
		tfIdOfDeleteProduct.setBounds(280, 275, 65, 20);
		produce.add(tfIdOfDeleteProduct);

		label = new JLabel("Id product:");
		label.setFont(new Font("Tahoma", Font.BOLD, 14));
		label.setBounds(160, 310, 115, 30);
		produce.add(label);

		tfidproductupdate = new JTextField();
		tfidproductupdate.setColumns(10);
		tfidproductupdate.setBounds(280, 315, 65, 20);
		produce.add(tfidproductupdate);

		label = new JLabel("Name:");
		label.setFont(new Font("Tahoma", Font.BOLD, 13));
		label.setBounds(160, 350, 45, 30);
		produce.add(label);

		tfmodifyproductname = new JTextField();
		tfmodifyproductname.setColumns(10);
		tfmodifyproductname.setBounds(210, 354, 65, 20);
		produce.add(tfmodifyproductname);

		label = new JLabel("Cost:");
		label.setFont(new Font("Tahoma", Font.BOLD, 13));
		label.setBounds(285, 350, 45, 30);
		produce.add(label);

		tfmodifyproductcost = new JTextField();
		tfmodifyproductcost.setColumns(10);
		tfmodifyproductcost.setBounds(330, 355, 65, 20);
		produce.add(tfmodifyproductcost);

		label = new JLabel("Amount:");
		label.setFont(new Font("Tahoma", Font.BOLD, 13));
		label.setBounds(405, 350, 45, 30);
		produce.add(label);

		tfmodifyproductamount = new JTextField();
		tfmodifyproductamount.setColumns(10);
		tfmodifyproductamount.setBounds(460, 354, 65, 20);
		produce.add(tfmodifyproductamount);

		label = new JLabel("ID (unique):");
		label.setFont(new Font("Tahoma", Font.BOLD, 13));
		label.setBounds(535, 350, 80, 30);
		produce.add(label);

		tfModifiedProducts = new JTextField();
		tfModifiedProducts.setColumns(10);
		tfModifiedProducts.setBounds(615, 355, 65, 20);
		produce.add(tfModifiedProducts);

		adminPage = new JPanel();
		adminPage.setBackground(SystemColor.inactiveCaption);
		adminPage.setBounds(10, 50, 705, 385);
		frame.getContentPane().add(adminPage);
		adminPage.setLayout(null);
		adminPage.setVisible(false);

		btnAdminAccounts = new JButton("Management Accounts");
		btnAdminAccounts.setBackground(Color.WHITE);
		btnAdminAccounts.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnAdminAccounts.setBounds(90, 185, 170, 45);
		adminPage.add(btnAdminAccounts);

		btnAdminProduce = new JButton("Management Products");
		btnAdminProduce.setBackground(Color.WHITE);
		btnAdminProduce.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnAdminProduce.setBounds(440, 185, 170, 45);
		adminPage.add(btnAdminProduce);

		label = new JLabel("Admin Page");
		label.setFont(new Font("Tahoma", Font.BOLD, 20));
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setBounds(200, 70, 290, 45);
		adminPage.add(label);
		adminPage.setVisible(false);

		myPage = new JPanel();
		myPage.setBackground(SystemColor.inactiveCaption);
		myPage.setBounds(10, 50, 705, 385);
		frame.getContentPane().add(myPage);
		myPage.setLayout(null);

		dtm3 = new DefaultTableModel(columnNames, 0);

		table3 = new JTable(dtm3);
		table3.setFillsViewportHeight(true);
		table3.setEnabled(false);
		scrollP2 = new JScrollPane(table3);
		scrollP2.setBounds(165, 95, 375, 240);
		myPage.add(scrollP2);

		label = new JLabel("Historic Commands:");
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setFont(new Font("Tahoma", Font.BOLD, 18));
		label.setBackground(new Color(240, 240, 240));
		label.setBounds(245, 11, 227, 33);
		myPage.add(label);
		myPage.setVisible(false);

		producePage = new JPanel();
		producePage.setBackground(SystemColor.inactiveCaption);
		producePage.setBounds(5, 45, 715, 385);
		frame.getContentPane().add(producePage);
		producePage.setLayout(null);
		producePage.setVisible(false);

		label = new JLabel("Products:");
		label.setBounds(40, 10, 120, 25);
		label.setFont(new Font("Tahoma", Font.BOLD, 14));
		producePage.add(label);

		dtm = new DefaultTableModel(columnNames, 0);

		table = new JTable(dtm);
		table.setFillsViewportHeight(true);
		table.setEnabled(false);
		scrollP = new JScrollPane(table);
		scrollP.setBounds(40, 60, 375, 160);
		produce.add(scrollP);

		dtm2 = new DefaultTableModel(columnNames, 0);

		table2 = new JTable(dtm2);
		table2.setFillsViewportHeight(true);
		table2.setEnabled(false);
		scrollP1 = new JScrollPane(table2);
		scrollP1.setBounds(40, 60, 375, 240);
		producePage.add(scrollP1);

		label = new JLabel("Search by:");
		label.setFont(new Font("Tahoma", Font.BOLD, 14));
		label.setBounds(455, 10, 120, 25);
		producePage.add(label);

		label = new JLabel("Minim:");
		label.setBounds(455, 85, 45, 15);
		producePage.add(label);

		minim = new JTextField();
		minim.setText("0");
		minim.setBounds(495, 85, 75, 20);
		producePage.add(minim);
		minim.setColumns(10);

		maxim = new JTextField();
		maxim.setText("0");
		maxim.setColumns(10);
		maxim.setBounds(615, 85, 75, 20);
		producePage.add(maxim);

		label = new JLabel(" Maxim:");
		label.setBounds(570, 85, 50, 15);
		producePage.add(label);

		minimAmount = new JTextField();
		minimAmount.setText("0");
		minimAmount.setColumns(10);
		minimAmount.setBounds(495, 140, 75, 20);
		producePage.add(minimAmount);

		label = new JLabel("Minim:");
		label.setBounds(455, 145, 45, 15);
		producePage.add(label);

		label = new JLabel(" Maxim:");
		label.setBounds(570, 145, 50, 15);
		producePage.add(label);

		maximAmount = new JTextField();
		maximAmount.setText("0");
		maximAmount.setColumns(10);
		maximAmount.setBounds(615, 140, 75, 20);
		producePage.add(maximAmount);

		nameOfSearch = new JTextField();
		nameOfSearch.setBounds(495, 195, 195, 20);
		producePage.add(nameOfSearch);
		nameOfSearch.setColumns(10);

		label = new JLabel("Name:");
		label.setBounds(455, 200, 45, 15);
		producePage.add(label);

		btnSearch = new JButton("Search Product");
		btnSearch.setBackground(Color.WHITE);
		btnSearch.setBounds(510, 245, 145, 25);
		producePage.add(btnSearch);

		label = new JLabel("Search by cost:");
		label.setFont(new Font("Tahoma", Font.BOLD, 14));
		label.setBounds(440, 65, 150, 15);
		producePage.add(label);

		label = new JLabel("Search by amount:");
		label.setFont(new Font("Tahoma", Font.BOLD, 14));
		label.setBounds(440, 125, 150, 15);
		producePage.add(label);

		radioBtnCost = new JRadioButton("");
		radioBtnCost.setBackground(SystemColor.inactiveCaption);
		radioBtnCost.setBounds(425, 80, 20, 25);
		producePage.add(radioBtnCost);

		label = new JLabel("Search by name:");
		label.setFont(new Font("Tahoma", Font.BOLD, 14));
		label.setBounds(440, 180, 150, 15);
		producePage.add(label);
		
		radioBtnAmount = new JRadioButton("");
		radioBtnAmount.setBackground(SystemColor.inactiveCaption);
		radioBtnAmount.setBounds(425, 140, 20, 25);
		producePage.add(radioBtnAmount);

		radioBtnName = new JRadioButton("");
		radioBtnName.setForeground(SystemColor.window);
		radioBtnName.setBackground(SystemColor.inactiveCaption);
		radioBtnName.setBounds(425, 195, 20, 25);
		producePage.add(radioBtnName);

		label = new JLabel("Amount:");
		label.setFont(new Font("Tahoma", Font.BOLD, 12));
		label.setBounds(125, 345, 75, 15);
		producePage.add(label);
		
		label = new JLabel("Total:");
		label.setFont(new Font("Tahoma", Font.BOLD, 12));
		label.setBounds(300, 345, 70, 15);
		producePage.add(label);
		
		label = new JLabel("ID:");
		label.setFont(new Font("Tahoma", Font.BOLD, 12));
		label.setBounds(40, 345, 30, 15);
		producePage.add(label);
		
		tfId = new JTextField("0");
		tfId.setBounds(65, 345, 45, 20);
		producePage.add(tfId);
		tfId.setColumns(10);
		
		label = new JLabel("Command: ( introduce ID product and amount desired)");
		label.setFont(new Font("Tahoma", Font.BOLD, 13));
		label.setBounds(40, 310, 445, 15);
		producePage.add(label);

		tfAmount = new JTextField("0");
		tfAmount.setBounds(195, 345, 85, 20);
		producePage.add(tfAmount);
		tfAmount.setColumns(10);

		btnCommand = new JButton("Command");
		btnCommand.setBackground(Color.WHITE);
		btnCommand.setBounds(590, 345, 115, 25);
		producePage.add(btnCommand);

		tfCost = new JTextField();
		tfCost.setBackground(SystemColor.inactiveCaption);
		tfCost.setEditable(false);
		tfCost.setBounds(360, 345, 85, 20);
		producePage.add(tfCost);
		tfCost.setColumns(10);
		
		btnCostTotal = new JButton("Calculate Cost");
		btnCostTotal.setFont(new Font("Tahoma", Font.BOLD, 10));
		btnCostTotal.setBackground(Color.WHITE);
		btnCostTotal.setBounds(455, 345, 120, 25);
		producePage.add(btnCostTotal);

		singInPage = new JPanel();
		singInPage.setBackground(SystemColor.inactiveCaption);
		singInPage.setBounds(5, 45, 715, 385);
		frame.getContentPane().add(singInPage);
		singInPage.setLayout(null);

		singInPanel = new JPanel();
		singInPanel.setBounds(205, 35, 295, 160);
		singInPage.add(singInPanel);
		singInPanel.setBackground(SystemColor.inactiveCaption);
		singInPanel.setLayout(null);
		singInPanel.setVisible(false);

		label = new JLabel("Already Sing in");
		label.setFont(new Font("Tahoma", Font.BOLD, 15));
		label.setBounds(45, 10, 210, 15);
		singInPanel.add(label);

		btnSingOut = new JButton("Singout");
		btnSingOut.setBackground(SystemColor.window);
		btnSingOut.setForeground(SystemColor.desktop);
		btnSingOut.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnSingOut.setBounds(95, 70, 110, 25);
		singInPanel.add(btnSingOut);

		tfAccountSingIn = new JTextField("");
		tfAccountSingIn.setFont(new Font("Tahoma", Font.BOLD, 12));
		tfAccountSingIn.setHorizontalAlignment(SwingConstants.CENTER);
		tfAccountSingIn.setEditable(false);
		tfAccountSingIn.setBackground(SystemColor.inactiveCaption);
		tfAccountSingIn.setBounds(110, 40, 85, 20);
		singInPanel.add(tfAccountSingIn);
		tfAccountSingIn.setColumns(10);

		windowsSingUp = new JPanel();
		windowsSingUp.setBackground(SystemColor.inactiveCaption);
		windowsSingUp.setBounds(10, 10, 295, 195);
		singInPage.add(windowsSingUp);
		windowsSingUp.setLayout(null);

		label = new JLabel("SING IN");
		label.setBounds(95, 20, 80, 20);
		windowsSingUp.add(label);
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setFont(new Font("Tahoma", Font.BOLD, 18));

		singIn = new JButton("Sing in");
		singIn.setBounds(40, 145, 220, 25);
		windowsSingUp.add(singIn);
		singIn.setBackground(SystemColor.white);
		singIn.setFont(new Font("Tahoma", Font.BOLD, 12));

		tfuser = new JTextField();
		tfuser.setBounds(135, 75, 125, 20);
		windowsSingUp.add(tfuser);
		tfuser.setText("");
		tfuser.setColumns(10);

		password = new JPasswordField();
		password.setBounds(135, 120, 125, 20);
		windowsSingUp.add(password);
		password.setText("");
		password.setColumns(10);

		label = new JLabel("User:");
		label.setBounds(40, 80, 45, 15);
		windowsSingUp.add(label);
		label.setFont(new Font("Tahoma", Font.BOLD, 15));

		label = new JLabel("Password:");
		label.setBounds(40, 120, 75, 15);
		windowsSingUp.add(label);
		label.setFont(new Font("Tahoma", Font.BOLD, 14));

		windowsSingIn = new JPanel();
		windowsSingIn.setBackground(SystemColor.inactiveCaption);
		windowsSingIn.setBounds(355, 10, 350, 365);
		singInPage.add(windowsSingIn);
		windowsSingIn.setLayout(null);

		label = new JLabel("CREATE NEW ACCOUNT");
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setFont(new Font("Tahoma", Font.BOLD, 15));
		label.setBounds(25, 10, 295, 60);
		windowsSingIn.add(label);

		label = new JLabel("Last name:");
		label.setFont(new Font("Tahoma", Font.BOLD, 12));
		label.setBounds(25, 100, 75, 15);
		windowsSingIn.add(label);

		label = new JLabel("First name:");
		label.setFont(new Font("Tahoma", Font.BOLD, 12));
		label.setBounds(25, 130, 75, 15);
		windowsSingIn.add(label);

		label = new JLabel("User Name:");
		label.setFont(new Font("Tahoma", Font.BOLD, 12));
		label.setBounds(25, 160, 75, 15);
		windowsSingIn.add(label);

		label = new JLabel("Password:");
		label.setFont(new Font("Tahoma", Font.BOLD, 12));
		label.setBounds(25, 195, 75, 15);
		windowsSingIn.add(label);

		label = new JLabel("Password:");
		label.setFont(new Font("Tahoma", Font.BOLD, 12));
		label.setBounds(25, 225, 75, 15);
		windowsSingIn.add(label);

		label = new JLabel("Mail:");
		label.setFont(new Font("Tahoma", Font.BOLD, 12));
		label.setBounds(25, 285, 75, 15);
		windowsSingIn.add(label);

		label = new JLabel("Telefon:");
		label.setFont(new Font("Tahoma", Font.BOLD, 12));
		label.setBounds(25, 255, 75, 15);
		windowsSingIn.add(label);

		createCont = new JButton("New Account");
		createCont.setFont(new Font("Tahoma", Font.BOLD, 12));
		createCont.setBackground(Color.WHITE);
		createCont.setBounds(70, 315, 205, 25);
		windowsSingIn.add(createCont);

		tfNewNameAccount = new JTextField();
		tfNewNameAccount.setColumns(10);
		tfNewNameAccount.setBounds(125, 95, 190, 20);
		windowsSingIn.add(tfNewNameAccount);

		tfNewFirstNameAccount = new JTextField();
		tfNewFirstNameAccount.setColumns(10);
		tfNewFirstNameAccount.setBounds(125, 125, 190, 20);
		windowsSingIn.add(tfNewFirstNameAccount);

		tfUsersNewAccount = new JTextField();
		tfUsersNewAccount.setColumns(10);
		tfUsersNewAccount.setBounds(125, 160, 190, 20);
		windowsSingIn.add(tfUsersNewAccount);

		tfNewAccountPassword = new JTextField();
		tfNewAccountPassword.setColumns(10);
		tfNewAccountPassword.setBounds(125, 190, 190, 20);
		windowsSingIn.add(tfNewAccountPassword);

		tfNewAccountPassword2 = new JTextField();
		tfNewAccountPassword2.setColumns(10);
		tfNewAccountPassword2.setBounds(125, 220, 190, 20);
		windowsSingIn.add(tfNewAccountPassword2);

		tfNewAccountMail = new JTextField();
		tfNewAccountMail.setColumns(10);
		tfNewAccountMail.setBounds(125, 285, 190, 20);
		windowsSingIn.add(tfNewAccountMail);

		tfNewAccountTel = new JTextField();
		tfNewAccountTel.setColumns(10);
		tfNewAccountTel.setBounds(125, 255, 190, 20);
		windowsSingIn.add(tfNewAccountTel);
		singInPage.setVisible(false);

		p1 = new JPanel();

		p1.setBackground(SystemColor.white);
		p1.setBounds(0, 0, 180, 35);
		frame.getContentPane().add(p1);
		p1.setLayout(null);

		btnSingInPage = new JButton("Sing In");
		btnSingInPage.setBackground(Color.WHITE);
		btnSingInPage.setBounds(10, 5, 165, 30);
		p1.add(btnSingInPage);

		p3 = new JPanel();
		p3.setBackground(SystemColor.white);

		p3.setBounds(365, 0, 180, 35);
		frame.getContentPane().add(p3);
		p3.setLayout(null);

		btnContactPage = new JButton("Contact");
		btnContactPage.setBackground(Color.WHITE);
		btnContactPage.setBounds(10, 5, 165, 30);
		p3.add(btnContactPage);

		p4 = new JPanel();
		p4.setBackground(SystemColor.white);
		p4.setBounds(545, 0, 180, 35);
		frame.getContentPane().add(p4);
		p4.setLayout(null);

		btnMyPage = new JButton("My Basket");
		btnMyPage.setBackground(Color.WHITE);
		btnMyPage.setBounds(10, 5, 165, 30);
		p4.add(btnMyPage);

		btnAdminPage = new JButton("Admin Page");
		btnAdminPage.setBounds(5, 5, 165, 30);
		p4.add(btnAdminPage);
		btnAdminPage.setVisible(false);

		p2 = new JPanel();
		p2.setBackground(SystemColor.white);

		p2.setBounds(180, 0, 185, 35);
		frame.getContentPane().add(p2);
		p2.setLayout(null);

		btnProducePage = new JButton("Products");
		btnProducePage.setBackground(Color.WHITE);
		btnProducePage.setBounds(10, 5, 165, 30);
		p2.add(btnProducePage);

		home = new JPanel();
		home.setBackground(SystemColor.inactiveCaption);
		homeImage = new JLabel();
		try {
			homeImage = new JLabel(new ImageIcon(ImageIO.read(new File("images/online-shopping.GIF"))));
		} catch (IOException e) {
			e.printStackTrace();
		}

		home.add(homeImage);
		home.setBounds(10, 50, 705, 385);
		frame.getContentPane().add(home);
		home.setVisible(true);

		frame.setVisible(true);
	}

	public void setTelefonContNou(String s) {
		this.tfNewAccountTel.setText(s);
	}

	public void setUserContNou(String s) {
		this.tfNewFirstNameAccount.setText(s);
	}

	public void setPrenumeContNou(String s) {
		this.tfNewFirstNameAccount.setText(s);
	}

	public void setparola2contnou(String s) {
		this.tfNewAccountPassword2.setText(s);
	}

	public void setParolaContNou(String s) {
		this.tfNewAccountPassword.setText(s);
	}

	public void setAdresaContNou(String s) {
		this.tfNewAccountMail.setText(s);
	}

	public void setNumeContNou(String s) {
		this.tfNewNameAccount.setText(s);
	}

	public String getNewAccountTelNumber() {
		return this.tfNewAccountTel.getText();
	}

	public String getNewAccountMail() {
		return this.tfNewAccountMail.getText();
	}

	public String getNewAccountPassword() {
		return this.tfNewAccountPassword.getText();
	}

	public String getNewAccountPassword2() {
		return this.tfNewAccountPassword2.getText();
	}

	public String getNewAccountFirstname() {
		return this.tfNewFirstNameAccount.getText();
	}

	public String getNewUserAccount() {
		return this.tfUsersNewAccount.getText();
	}

	public String getNewAccountName() {
		return this.tfNewNameAccount.getText();
	}

	public void setColorPage1(int i) {
		if (i == 1)
			p1.setBackground(SystemColor.inactiveCaption);
		else
			p1.setBackground(SystemColor.white);
	}

	public void setColorPage2(int i) {
		if (i == 1)
			p2.setBackground(SystemColor.inactiveCaption);
		else
			p2.setBackground(SystemColor.white);
	}

	public void setColorPage3(int i) {
		if (i == 1)
			p3.setBackground(SystemColor.inactiveCaption);
		else
			p3.setBackground(SystemColor.white);
	}

	public void setColorPage4(int i) {
		if (i == 1)
			p4.setBackground(SystemColor.inactiveCaption);
		else
			p4.setBackground(SystemColor.white);
	}

	public void setColorPageB1(int i) {
		if (i == 1)
			btnSingInPage.setBackground(SystemColor.inactiveCaption);
		else
			btnSingInPage.setBackground(SystemColor.white);
	}

	public void setColorPageB2(int i) {
		if (i == 1)
			btnProducePage.setBackground(SystemColor.inactiveCaption);
		else
			btnProducePage.setBackground(SystemColor.white);
	}

	public void setColorPageB3(int i) {
		if (i == 1)
			btnContactPage.setBackground(SystemColor.inactiveCaption);
		else
			btnContactPage.setBackground(SystemColor.white);
	}

	public void setColorPageB4(int i) {
		if (i == 1)
			btnMyPage.setBackground(SystemColor.inactiveCaption);
		else
			btnMyPage.setBackground(SystemColor.white);
	}

	public void setColorPageB5(int i) {
		if (i == 1)
			btnAdminPage.setBackground(SystemColor.inactiveCaption);
		else
			btnAdminPage.setBackground(SystemColor.white);
	}

	public String getPassword() {
		return this.password.getText();
	}

	public String getUser() {
		return this.tfuser.getText();
	}

	public void addProductsListeners(ActionListener a) {
		btnProducePage.addActionListener(a);
	}

	public void addTrainingUpdateListeners(ActionListener a) {
		btnTainingUpdate.addActionListener(a);
	}

	public void addContactListeners(ActionListener a) {
		btnContactPage.addActionListener(a);
	}

	public void addMyPageListeners(ActionListener a) {
		btnMyPage.addActionListener(a);
	}

	public void addSingInPageListeners(ActionListener a) {
		btnSingInPage.addActionListener(a);
	}

	public void addAdminPageListeners(ActionListener a) {
		btnAdminPage.addActionListener(a);
	}

	public void addSearchListeners(ActionListener a) {
		btnSearch.addActionListener(a);
	}

	public void addCalculPretListeners(ActionListener a) {
		btnCostTotal.addActionListener(a);
	}

	public void addAfterLoginListeners(ActionListener a) {
		singIn.addActionListener(a);
	}

	public void addContNouListeners(ActionListener a) {
		createCont.addActionListener(a);
	}

	public void addComandaListeners(ActionListener a) {
		btnCommand.addActionListener(a);
	}

	public void addCostCommand(ActionListener a) {
		btnCostTotal.addActionListener(a);
	}

	public void addCommand(ActionListener a) {
		btnCommand.addActionListener(a);
	}

	public void addSingIn(ActionListener a) {
		singIn.addActionListener(a);
	}

	public void addNewAccount(ActionListener a) {
		createCont.addActionListener(a);
	}

	public void addSingOut(ActionListener a) {
		btnSingOut.addActionListener(a);
	}

	public void addAdminProducts(ActionListener a) {
		btnAdminProduce.addActionListener(a);
	}

	public void addAddProducts(ActionListener a) {
		btnAddProduce.addActionListener(a);
	}

	public void addSubProducts(ActionListener a) {
		btnSubProduce.addActionListener(a);
	}

	public void addModificationProduct(ActionListener a) {
		btnFinalUpdate.addActionListener(a);
	}

	////////////////////////////////////////////////////////////////////client
	public void addNewPage(ActionListener a) {
		btnAdminAccounts.addActionListener(a);
	}
	public int getIdUpdate() {
		return (int) Integer.parseInt(this.tftraining.getText());
	}
	public void addAddClient(ActionListener a) {
		btnAddCustomer.addActionListener(a);
	}

	public void addTrainingUpdateCLNLs(ActionListener a) {
		btnNewButton.addActionListener(a);
	}

	public void addupdateClient(ActionListener a) {
		btnUpdate.addActionListener(a);
	}

	public JTextField getTname() {
		return tfName;
	}

	public void setFfffnume(JTextField tfName) {
		this.tfName = tfName;
	}

	public JTextField getFFirstname() {
		return tfFirstName;
	}

	public void setFfffprenume(JTextField tfFirstName) {
		this.tfFirstName = tfFirstName;
	}

	public JTextField getFUser() {
		return tfuser1;
	}

	public void setFfffuser(JTextField tfuser) {
		this.tfuser1 = tfuser;
	}

	public JTextField getFTelnumber() {
		return tftel;
	}

	public void setFffftelefon(JTextField tftel) {
		this.tftel = tftel;
	}

	public JTextField getFmail() {
		return tfmail;
	}

	public void setFfffmail(JTextField tfmail) {
		this.tfmail = tfmail;
	}

	public JButton getBtnUpdate() {
		return btnUpdate;
	}

	public void setBtnUpdate(JButton btnUpdate) {
		this.btnUpdate = btnUpdate;
	}

	public String getShowname1() {
		return tfname1.getText();
	}

	public void setShowname1(String tfname1) {
		this.tfname1.setText(tfname1);
	}

	

	public String getShowFirstname1() {
		return tffirstname1.getText();
	}

	public void setShowFirstname1(String tffirstname1) {
		this.tffirstname1.setText(tffirstname1);
	}

	public String getShowUser1() {
		return tfuser2.getText();
	}

	public void setShowUser1(String tfuser1) {
		this.tfuser2.setText(tfuser1);
	}

	public String getShowTel1() {
		return tftel1.getText();
	}

	public void setShowTel1(String s) {
		this.tftel1.setText(s);
	}

	public String getShowMail1() {
		return tfmail1.getText();
	}

	public void getShowMail1(String tfmail1) {
		this.tfmail1.setText(tfmail1);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////// product

	public void setVisNewPage(boolean vis) {
		customer.setVisible(vis);
	}

	public void setVisHome(boolean vis) {
		home.setVisible(vis);
	}

	public void setVisMyPage(boolean vis) {
		myPage.setVisible(vis);
	}

	public void setInvProduse(boolean inv) {
		singInPage.setVisible(inv);
	}

	public void setVisProductsPage(boolean vis) {
		producePage.setVisible(vis);
	}

	public void setVisAdmin(boolean vis) {
		adminPage.setVisible(vis);
	}

	public void setVisAdminProduct(boolean vis) {
		produce.setVisible(vis);
	}

	public void setVisContact(boolean vis) {
		contactPage.setVisible(vis);
	}

	public void setVisSingIn(boolean vis) {
		singInPage.setVisible(vis);
	}

	public boolean getRDBName() {
		return this.radioBtnName.isSelected();
	}
	
	public boolean getRDBAmount() {
		return this.radioBtnAmount.isSelected();
	}

	public boolean getRDBCost() {
		return this.radioBtnCost.isSelected();
	}

	
	public void setNullTable5() {

		dtm.setNumRows(0);

	}

	public void setNewRow4(String a, String b, String c, String d, String e) {

		dtm4.addRow(new Object[] { a, b, c, d, e });
		table4 = new JTable(dtm4);
	}

	public void setNullTable2() {

		dtm.setNumRows(0);

	}
	
	  public void setNewRowAdmin(int ind,Products p){
	 
	  dtm.addRow(new Object[] { ind, p.getName(), p.getCost(),
	  p.getAmount()}); table = new JTable(dtm); }
	  
	 
	 

	public void setShowNumeProdus(String s) {
		tfmodifyproductname.setText(s);
	}

	public void setShowPret(double x) {
		String s = Double.toString(x);
		this.tfmodifyproductcost.setText(s);
	}

	public void setShowCantit(int x) {
		String s = Integer.toString(x);
		this.tfmodifyproductamount.setText(s);
	}

	public void setShowID(int x) {
		String s = Integer.toString(x);
		this.tfModifiedProducts.setText(s);
	}

	public String getShowProductName() {
		return tfmodifyproductname.getText();
	}

	public double getShowCost() {
		return Double.parseDouble(tfmodifyproductcost.getText());
	}

	public int getShowAmount() {
		return Integer.parseInt(tfmodifyproductamount.getText());
	}

	public int getShowID() {
		return Integer.parseInt(tfModifiedProducts.getText());
	}

	public void setNullTable() {

		dtm2.setNumRows(0);

	}
	public void setNullTable4() {

		dtm4.setNumRows(0);

	}
	
	  public void setNewRow(int ind,Products p){
	  
	  dtm2.addRow(new Object[] { ind, p.getName(), p.getCost(),
	  p.getAmount()}); table2 = new JTable(dtm2); 
		  }
	  
	  
	  
	  
	  
	  public void setNewRow3(int ind,Products p){
	  
	  dtm3.addRow(new Object[] { ind, p.getName(), p.getCost(),
	  p.getAmount()}); table3 = new JTable(dtm3); 
	  }
	  
	  
	public void setNullTable3() {

		dtm3.setNumRows(0);

	}

	public double getMinimCost() {
		String s = minim.getText();
		return (double) Double.parseDouble(s);
	}

	public double getMaximCost() {
		String s = maxim.getText();
		return (double) Double.parseDouble(s);
	}

	public int getMaximAmount() {
		String s = this.maximAmount.getText();
		return (int) Integer.parseInt(s);
	}

	public int getMinimAmount() {
		String s = this.minimAmount.getText();
		return (int) Integer.parseInt(s);
	}

	public String getSearchName() {
		return this.nameOfSearch.getText();
	}

	public int getRequiredAmount() {
		return (int) Integer.parseInt(tfAmount.getText());
	}

	public int getIdRequired() {
		return (int) Integer.parseInt(tfId.getText());
	}

	public void setCostCalculat(double a) {
		String s = Double.toString(a);
		this.tfCost.setText(s);
	}

	public void setLogat(boolean a) {
		this.logat = a;
	}

	public boolean getLogat() {
		return this.logat;
	}

	public void setAdmin(boolean x) {
		this.adminn = x;
	}

	public boolean getAdmin() {
		return this.adminn;
	}

	public String getNewProductName() {
		return this.tfAddNewProduceName.getText();
	}

	public int getNewProductAmount() {
		return Integer.parseInt(this.tfAddNewProduceAmount.getText());
	}

	public double getNewProductCost() {
		return Double.parseDouble(this.tfAddNewProduceCost.getText());
	}

	public int getNewProductID() {
		return Integer.parseInt(this.tfNewIdOfProducts.getText());
	}

	public int getIdDeleteProduct() {
		return Integer.parseInt(this.tfIdOfDeleteProduct.getText());
	}

	public int getIdREmove() {
		return (int) Integer.parseInt(this.tfidproductupdate.getText());
	}

	public void setVisInreg(boolean a) {
		windowsSingIn.setVisible(a);
	}

	public void setVisLog(boolean a) {
		windowsSingUp.setVisible(a);
	}

	public void setAccountSingin(String s) {
		this.tfAccountSingIn.setText(s);
	}

	public void setSingInPanel(boolean vis) {
		this.singInPanel.setVisible(vis);
	}

	public void setVisMyPageButton(boolean vis) {
		this.btnMyPage.setVisible(vis);
	}

	public void setVisButtonAdmin(boolean vis) {
		this.btnAdminPage.setVisible(vis);
	}

	public void setVizButtonMP(boolean vis) {
		this.btnMyPage.setVisible(vis);
	}

	public void setListener(WindowListener e) {
		frame.addWindowListener(e);
	}
}
